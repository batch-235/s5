const { exchangeRates, isPropertiesComplete, isValidObject, doesNotExist } = require('../src/util.js');
const express = require("express");
const router = express.Router();

router.get('/rates', (req, res) => {
	return res.status(200).send(exchangeRates);
})

router.post('/currency', (req, res) => {

	if (!isPropertiesComplete(req.body).status) {
		return res.status(400).send({
            error: `Bad Request - missing required property ${isPropertiesComplete(req.body).missing}`
        });
	}

	if (typeof req.body.name !== "string" || req.body.name === '') {
		return res.status(400).send({
            error: `Bad Request - invalid property NAME`
        });
	}

	if (!isValidObject(req.body.ex).status) {
		return res.status(400).send({
            error: `Bad Request - ${isValidObject(req.body).message}`
        });
	}

	if (typeof req.body.alias !== "string" || req.body.alias === '') {
		return res.status(400).send({
            error: `Bad Request - invalid property ALIAS`
        });
	}

	if (!doesNotExist(req.body.alias, exchangeRates)) {
		return res.status(400).send({
            error: `Bad Request - ALIAS already exists in our data base`
        });
	}

	return res.status(200).send({
		success: 'Currency added'
	})

})

module.exports = router;
