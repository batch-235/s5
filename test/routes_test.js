const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/getRates')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/forex/rates')
		.end((err, res) => {
			assert.equal(res.status,200)
			done();	
		})		
	})
	

	it('test_api_post_currency_returns_200_if_complete_input_given', () => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
		})
	})

})



describe("/currency Test Suite", () => {

	const URL = "http://localhost:5001";
	const ENDPOINT = "/forex/currency";

	it("[1] Test API endpoint /currency if running", (done) => {
		chai.request(URL)
		.get(ENDPOINT)
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})

	it("[2] Check if post /currency returns status 400 if name is missing", (done) => {
		chai.request(URL)
		.post(ENDPOINT)
		.type("json")
		.send({
			'alias': 'test',
			// 'name': 'United States Dollar',
			'ex': {
				'peso': 50.73,
				'won': 1187.24,
				'yen': 108.63,
				'yuan': 7.03
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("[3] Check if post /currency returns status 400 if name is not a string", (done) => {
		chai.request(URL)
		.post(ENDPOINT)
		.type("json")
		.send({
			'alias': 'test',
			'name': ['Test Currency'],
			'ex': {
				'peso': 50.73,
				'won': 1187.24,
				'yen': 108.63,
				'yuan': 7.03
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("[4] Check if post /currency returns status 400 if name is empty string", (done) => {
		chai.request(URL)
		.post(ENDPOINT)
		.type("json")
		.send({
			'alias': 'test',
			'name': '',
			'ex': {
				'peso': 50.73,
				'won': 1187.24,
				'yen': 108.63,
				'yuan': 7.03
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("[5] Check if post /currency returns status 400 if ex is missing", (done) => {
		chai.request(URL)
		.post(ENDPOINT)
		.type("json")
		.send({
			'alias': 'test',
			'name': 'Test Currency'
			// 'ex': {
			// 	'peso': 50.73,
			// 	'won': 1187.24,
			// 	'yen': 108.63,
			// 	'yuan': 7.03
			// }
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("[6] Check if post /currency returns status 400 if ex is not an object", (done) => {
		chai.request(URL)
		.post(ENDPOINT)
		.type("json")
		.send({
			'alias': 'test',
			'name': 'Test Currency',
			'ex': ['peso','won','yen']
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("[7] Check if post /currency returns status 400 if ex is empty object", (done) => {
		chai.request(URL)
		.post(ENDPOINT)
		.type("json")
		.send({
			'alias': 'test',
			'name': 'Test Currency',
			'ex': {}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("[8] Check if post /currency returns status 400 if alias is missing", (done) => {
		chai.request(URL)
		.post(ENDPOINT)
		.type("json")
		.send({
			// 'alias': 'usd',
			'name': 'Test Currency',
			'ex': {
				'peso': 50.73,
				'won': 1187.24,
				'yen': 108.63,
				'yuan': 7.03
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("[9] Check if post /currency returns status 400 if alias is not a string", (done) => {
		chai.request(URL)
		.post(ENDPOINT)
		.type("json")
		.send({
			'alias': ['test'],
			'name': 'Test Currency',
			'ex': {
				'peso': 50.73,
				'won': 1187.24,
				'yen': 108.63,
				'yuan': 7.03
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("[10] Check if post /currency returns status 400 if alias is empty string", (done) => {
		chai.request(URL)
		.post(ENDPOINT)
		.type("json")
		.send({
			'alias': '',
			'name': 'Test Currency',
			'ex': {
				'peso': 50.73,
				'won': 1187.24,
				'yen': 108.63,
				'yuan': 7.03
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("[11] Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias", (done) => {
		chai.request(URL)
		.post(ENDPOINT)
		.type("json")
		.send({
			'alias': 'usd',
			'name': 'United States Dollar',
			'ex': {
				'peso': 50.73,
				'won': 1187.24,
				'yen': 108.63,
				'yuan': 7.03
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	})

	it("[12] Check if post /currency returns status 200 if all fields are complete and there are no duplicates", (done) => {
		chai.request(URL)
		.post(ENDPOINT)
		.type("json")
		.send({
			'alias': 'eur',
			'name': 'Euro',
			'ex': {
				'peso': 100,
				'won': 200,
				'yen': 300,
				'yuan': 400
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 200);
			done();
		})
	})

})
