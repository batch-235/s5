const exchangeRates = [
    {
    	'alias': 'usd',
      	'name': 'United States Dollar',
      	'ex': {
        	'peso': 50.73,
        	'won': 1187.24,
        	'yen': 108.63,
        	'yuan': 7.03
    	}
    },
    {
    	'alias': 'yen',
      	'name': 'Japanese Yen',
      	'ex': {
	        'peso': 0.47,
	        'usd': 0.0092,
	        'won': 10.93,
	        'yuan': 0.065
      	}
    },
    {
    	'alias': 'php',
      	'name': 'Philippine Peso',
      	'ex':{
	        'usd': 0.020,
	        'won': 23.39,
	        'yen': 2.14,
	        'yuan': 0.14
      	}
    },
    {
    	'alias': 'yuan',
      	'name': 'Chinese Yuan',
      	'ex': {
	        'peso': 7.21,
	        'usd': 0.14,
	        'won': 168.85,
	        'yen': 15.45
      	}
    },
    {
    	'alias':'won',
      	'name': 'South Korean Won',
      	'ex': {
	        'peso': 0.043,
	        'usd': 0.00084,
	        'yen': 0.092,
	        'yuan': 0.0059
      	}
    }
];

// function exchange(baseCurrency, targetCurrency, amount){
// 	return exchangeRates[baseCurrency].ex[targetCurrency] * amount;
// }



// Function that checks if the properties of the request body matches all the expected properties for the post method

const isPropertiesComplete = (obj) => {
	const properties = ['alias','name','ex'];
    const data = Object.keys(obj);

    // default result status
    let result = { status: true };

    // change the status to false if there is a missing property in the request body and add the missing property to the result object
    properties.forEach(property => {
        if(!data.includes(property)) result = { status: false, missing: property };
    })

    return result;
}


// Function that checks if an object is a valid object
// 	- must be a valid object only, will disregard array data type because its also an object
//  - must not be empty {}
const isValidObject = (obj) => {

	let result = { status: true };

	if (Array.isArray(obj) && obj !== null) {
		result.status = false;
		result.message = "Ex must be a valid object literal";
		return result;
	}

	if (Object.keys(obj).length === 0) {
		result.status = false;
		result.message = "Ex must not be an empty object"
		return result;
	}

	return result;

}


// Function that checks the alis of the request body if it does not exist in our mock data base
const doesNotExist = (reqAlias, database) => {

	let result = database.find(currency => currency.alias === reqAlias);

	return result === undefined ? true : false
}


module.exports = {
	exchangeRates: exchangeRates,
	// exchange: exchange,
	isPropertiesComplete: isPropertiesComplete,
	isValidObject: isValidObject,
	doesNotExist: doesNotExist
}